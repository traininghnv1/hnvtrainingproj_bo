package com.hnv.api.service.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.hnv.api.def.DefTime;
import com.hnv.common.tool.ToolLogServer;
import com.hnv.common.util.CacheData;
import com.hnv.db.aut.TaAutUser;

@Service
public class ServiceAPILoginCheck implements UserDetailsService {
	public static BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String password = reqAutUserPassword(username);
		if (password!=null) {
			User sprUser = new User(username, password,	new ArrayList<>());
			return sprUser;
		}else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
	
	private static CacheData<TaAutUser> 	cache_dbUser	= new CacheData<TaAutUser> 	(500, DefTime.TIME_SLEEP_00_30_00_000);
	private static Hashtable<String,String> cache_Pass		= new Hashtable<String,String>();
	
	public static String reqAutUserPassword(String uName) {
		try {
			//---password = reqHash256(reqHash256(ui Pass) + salt)
			//--for test ------------------------------------------------------------------
			//password = ToolSecurity.reqHash256(password);
			//password = ToolSecurity.reqHash256(password + salt);
			//-----------------------------------------------------------------------------
			if (uName==null) return null;
			
			String		dbPass 	= cache_Pass.get(uName);
			TaAutUser 	user 	= null; 
			if (dbPass!=null) {
				user = cache_dbUser.reqCheckIfOld(uName);
				if (user!=null) 
					cache_Pass.remove(uName);
				else 
					return dbPass;
			}
			
			Criterion cri 	= Restrictions.and(	
					Restrictions.eq(TaAutUser.ATT_T_LOGIN_01, uName),
					Restrictions.eq(TaAutUser.ATT_I_STATUS, TaAutUser.STAT_ACTIVE),
					Restrictions.or(Restrictions.isNull(TaAutUser.ATT_D_DATE_03), Restrictions.gt(TaAutUser.ATT_D_DATE_03, new Date()))
					);

			List<TaAutUser>users = TaAutUser.DAO.reqList(cri);
			if(users != null && users.size()>0) {
				user 	= users.get(0);
				dbPass 	= (String)user.req(TaAutUser.ATT_T_PASS_01).toString();
				dbPass 	= bcryptEncoder.encode(dbPass);
				
				user.reqSet(TaAutUser.ATT_T_PASS_01, null);
				user.reqSet(TaAutUser.ATT_T_PASS_02, null);
				
				cache_dbUser.reqPut(uName, user);
				cache_Pass	.put(uName, dbPass);
				
				return 		dbPass;		
			}
			
			
			//----sign-on with google account
			cri 	= Restrictions.and(	
					Restrictions.eq(TaAutUser.ATT_T_LOGIN_02, uName),
					Restrictions.eq(TaAutUser.ATT_I_STATUS, TaAutUser.STAT_ACTIVE),
					Restrictions.or(Restrictions.isNull(TaAutUser.ATT_D_DATE_03), Restrictions.gt(TaAutUser.ATT_D_DATE_03, new Date()))
					);

			users = TaAutUser.DAO.reqList(cri);
			if(users != null && users.size()>0) {
				user 	= users.get(0);
				dbPass 	= (String)user.req(TaAutUser.ATT_T_PASS_02).toString();
				dbPass 	= bcryptEncoder.encode(dbPass);
				
				user.reqSet(TaAutUser.ATT_T_PASS_01, null);
				user.reqSet(TaAutUser.ATT_T_PASS_02, null);
				
				cache_dbUser.reqPut(uName, user);
				cache_Pass	.put(uName, dbPass);
				
				return 		dbPass;		
			}
			
		} catch (Exception e) {
			ToolLogServer.doLogErr(e);
		}
		return null;
	}

	public static TaAutUser reqAutUser(String uName) {
		if (uName==null) return null;
		TaAutUser u = cache_dbUser.reqData(uName);
		try{
			if (u!=null) {
				u.doBuilRoleRighs(false);
//				u.doBuildManager();
				u.doBuildAvatar(false);
				u.doBuildPerson(false);
			}
		} catch (Exception e) {
			ToolLogServer.doLogErr(e);
		}
		return u;
	}
}