package com.hnv.db.nso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.criterion.Restrictions;

import com.hnv.api.def.DefDBExt;
import com.hnv.api.main.Hnv_CfgHibernate;
import com.hnv.db.EntityAbstract;
import com.hnv.db.EntityDAO;
import com.hnv.db.aut.TaAutUser;
import com.hnv.db.tpy.TaTpyCategoryEntity;
import com.hnv.db.tpy.TaTpyDocument;

/**
 * TaNsoPost by H&V SAS
 */
@Entity
@Table(name = DefDBExt.TA_NSO_POST )
public class TaNsoPost extends EntityAbstract<TaNsoPost> {
	public static final double EVAL_MIN = 1.0;
	public static final double EVAL_MAX = 5.0;

	private static final long serialVersionUID = 1L;

	//---------------------------List of Column from DB-----------------------------
	public static final String	COL_I_ID                              =	"I_ID";

	public static final String	COL_T_REF                             =	"T_Ref";
	public static final String	COL_T_TITLE                           =	"T_Title";
	public static final String	COL_T_CONTENT_01                      =	"T_Content_01";
	public static final String	COL_T_CONTENT_02                      =	"T_Content_02";
	public static final String	COL_T_CONTENT_03                      =	"T_Content_03";
	public static final String	COL_T_CONTENT_04                      =	"T_Content_04";
	public static final String	COL_T_CONTENT_05                      =	"T_Content_05";
	public static final String	COL_T_CONTENT_06                      =	"T_Content_06";
	public static final String	COL_T_CONTENT_07                      =	"T_Content_07";
	public static final String	COL_T_CONTENT_08                      =	"T_Content_08";
	public static final String	COL_T_CONTENT_09                      =	"T_Content_09";
	public static final String	COL_T_CONTENT_10                      =	"T_Content_10";
	
	
	public static final String	COL_I_TYPE_01                         =	"I_Type_01";
	public static final String	COL_I_TYPE_02                         =	"I_Type_02";
	public static final String	COL_I_TYPE_03                         =	"I_Type_03";
	public static final String	COL_I_STATUS                          =	"I_Status";
	public static final String	COL_T_COMMENT                         =	"T_Comment";

	public static final String	COL_D_DATE_NEW                        =	"D_Date_New";
	public static final String	COL_D_DATE_MOD                        =	"D_Date_Mod";
	public static final String	COL_I_AUT_USER_NEW                    =	"I_Aut_User_New";
	public static final String	COL_I_AUT_USER_MOD                    =	"I_Aut_User_Mod";
	
	public static final String	COL_I_NB_RESP                         =	"I_Nb_Resp";
	public static final String	COL_I_PARENT                          =	"I_Parent";
	public static final String	COL_I_ENTITY_TYPE                     =	"I_Entity_Type";
	public static final String	COL_I_ENTITY_ID                       =	"I_Entity_ID";
	

	public static final String	COL_T_PROPERTY_01                     =	"T_Property_01";
	public static final String	COL_T_PROPERTY_02                     =	"T_Property_02";
	public static final String	COL_T_PROPERTY_03                     =	"T_Property_03";
	public static final String	COL_T_PROPERTY_04                     =	"T_Property_04";
	
	public static final String	COL_F_EVAL_01                         =	"F_Eval_01";
	public static final String	COL_F_EVAL_02                         =	"F_Eval_02";
	public static final String	COL_F_EVAL_03                         =	"F_Eval_03";
	public static final String	COL_F_EVAL_04                         =	"F_Eval_04";
	public static final String	COL_F_EVAL_05                         =	"F_Eval_05";
	//---------------------------List of ATTR of class-----------------------------
	public static final String	ATT_I_ID                              =	"I_ID";
	public static final String	ATT_T_REF                             =	"T_Ref";
	public static final String	ATT_T_TITLE                           =	"T_Title";
	
	public static final String	ATT_T_CONTENT_01                      =	"T_Content_01";
	public static final String	ATT_T_CONTENT_02                      =	"T_Content_02";
	public static final String	ATT_T_CONTENT_03                      =	"T_Content_03";
	public static final String	ATT_T_CONTENT_04                      =	"T_Content_04";
	public static final String	ATT_T_CONTENT_05                      =	"T_Content_05";
	public static final String	ATT_T_CONTENT_06                      =	"T_Content_06";
	public static final String	ATT_T_CONTENT_07                      =	"T_Content_07";
	public static final String	ATT_T_CONTENT_08                      =	"T_Content_08";
	public static final String	ATT_T_CONTENT_09                      =	"T_Content_09";
	public static final String	ATT_T_CONTENT_10                      =	"T_Content_10";
	
	public static final String	ATT_I_TYPE_01                         =	"I_Type_01";
	public static final String	ATT_I_TYPE_02                         =	"I_Type_02";
	public static final String	ATT_I_TYPE_03                         =	"I_Type_03";
	public static final String	ATT_I_STATUS                          =	"I_Status";
	public static final String	ATT_T_COMMENT                         =	"T_Comment";
	
	public static final String	ATT_D_DATE_NEW                        =	"D_Date_New";
	public static final String	ATT_D_DATE_MOD                        =	"D_Date_Mod";
	public static final String	ATT_I_AUT_USER_NEW                    =	"I_Aut_User_New";
	public static final String	ATT_I_AUT_USER_MOD                    =	"I_Aut_User_Mod";
	
	public static final String	ATT_I_NB_RESP                         =	"I_Nb_Resp";
	public static final String	ATT_I_PARENT                          =	"I_Parent";
	public static final String	ATT_I_ENTITY_TYPE                     =	"I_Entity_Type";
	public static final String	ATT_I_ENTITY_ID                       =	"I_Entity_ID";
	
	public static final String	ATT_T_PROPERTY_01                     =	"T_Property_01";
	public static final String	ATT_T_PROPERTY_02                     =	"T_Property_02";
	public static final String	ATT_T_PROPERTY_03                     =	"T_Property_03";
	public static final String	ATT_T_PROPERTY_04                     =	"T_Property_04";

	public static final String	ATT_F_EVAL_01                         =	"F_Eval_01";
	public static final String	ATT_F_EVAL_02                         =	"F_Eval_02";
	public static final String	ATT_F_EVAL_03                         =	"F_Eval_03";
	public static final String	ATT_F_EVAL_04                         =	"F_Eval_04";
	public static final String	ATT_F_EVAL_05                         =	"F_Eval_05";
	
	public static final String	ATT_O_DOCUMENTS                       =	"O_Documents";
	public static final String	ATT_O_USER                            =	"O_User";
	public static final String	ATT_O_LOGIN                           =	"O_Login";
	public static final String	ATT_O_POST                           =	"O_Posts";
	public static final String	ATT_O_VIEW                           =	"O_View";

	
//	private static final String POST_ANONYMOUS						  = "nso_post_anonymous";

	public static final String	ATT_O_TRANSLATIONS                	  =	"O_Translations";
	
	public static final String	ATT_O_CHILDS                	  	  =	"O_Childs";
	
	public static final String	ATT_O_CATS                      	  =	"O_Cats";

	//-------every entity class must initialize its DAO from here -----------------------------
	private 	static 	final boolean[] 			RIGHTS		= {true, true, true, true, false}; //canRead, canAdd, canUpd, canDel, del physique or flag only 
	private 	static 	final boolean[]				HISTORY		= {false, false, false}; //add, mod, del

	public		static 	final EntityDAO<TaNsoPost> 	DAO;

	//---------------------------------------------------------------------------------------------------------------------------------------
	public static final int POST_STATUS_DRAFT				= 0;
	public static final int POST_STATUS_REVIEW				= 1;
	public static final int POST_STATUS_VALIDATED			= 2;
	public static final int POST_STATUS_REPORTED			= 3;
	public static final int POST_STATUS_DELETED				= 4;

	public static final int POST_TYPE_OFFER					= 100;
	public static final int POST_TYPE_BLOG					= 101;
	public static final int POST_TYPE_EVAL					= 102;
	
	static{
		DAO = new EntityDAO<TaNsoPost>(Hnv_CfgHibernate.reqFactoryEMSession(Hnv_CfgHibernate.ID_FACT_MAIN) , TaNsoPost.class,RIGHTS, HISTORY, DefDBExt.TA_NSO_POST, DefDBExt.ID_TA_NSO_POST);
	}

	//-----------------------Class Attributs-------------------------
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name=COL_I_ID, nullable = false)
	private	Integer         I_ID;

	@Column(name=COL_F_EVAL_01, nullable = true)
	private	Double          F_Eval_01;

	@Column(name=COL_F_EVAL_02, nullable = true)
	private	Double          F_Eval_02;

	@Column(name=COL_F_EVAL_03, nullable = true)
	private	Double          F_Eval_03;

	@Column(name=COL_F_EVAL_04, nullable = true)
	private	Double          F_Eval_04;

	@Column(name=COL_F_EVAL_05, nullable = true)
	private	Double          F_Eval_05;

	@Column(name=COL_T_REF, nullable = true)
	private	String          T_Ref;

	@Column(name=COL_T_TITLE, nullable = true)
	private	String          T_Title;

	@Column(name=COL_T_CONTENT_01, nullable = true)
	private	String          T_Content_01;

	@Column(name=COL_T_CONTENT_02, nullable = true)
	private	String          T_Content_02;

	@Column(name=COL_T_CONTENT_03, nullable = true)
	private	String          T_Content_03;

	@Column(name=COL_T_CONTENT_04, nullable = true)
	private	String          T_Content_04;

	@Column(name=COL_T_CONTENT_05, nullable = true)
	private	String          T_Content_05;

	@Column(name=COL_T_CONTENT_06, nullable = true)
	private	String          T_Content_06;

	@Column(name=COL_T_CONTENT_07, nullable = true)
	private	String          T_Content_07;

	@Column(name=COL_T_CONTENT_08, nullable = true)
	private	String          T_Content_08;

	@Column(name=COL_T_CONTENT_09, nullable = true)
	private	String          T_Content_09;

	@Column(name=COL_T_CONTENT_10, nullable = true)
	private	String          T_Content_10;

	

	@Column(name=COL_I_STATUS, nullable = true)
	private	Integer         I_Status;

	@Column(name=COL_T_COMMENT, nullable = true)
	private	String          T_Comment;

	@Column(name=COL_D_DATE_NEW, nullable = true)
	private	Date            D_Date_New;

	@Column(name=COL_D_DATE_MOD, nullable = true)
	private	Date            D_Date_Mod;

	@Column(name=COL_I_AUT_USER_NEW, nullable = true)
	private	Integer         I_Aut_User_New;
	
	@Column(name=COL_I_AUT_USER_MOD, nullable = true)
	private	Integer         I_Aut_User_Mod;

	@Column(name=COL_I_NB_RESP, nullable = true)
	private	Integer         I_Nb_Resp;

	@Column(name=COL_I_PARENT, nullable = true)
	private	Integer         I_Parent;

	@Column(name=COL_I_ENTITY_TYPE, nullable = true)
	private	Integer         I_Entity_Type;

	@Column(name=COL_I_ENTITY_ID, nullable = true)
	private	Integer         I_Entity_ID;

	@Column(name=COL_I_TYPE_01, nullable = true)
	private	Integer         I_Type_01;

	@Column(name=COL_I_TYPE_02, nullable = true)
	
	private	Integer         I_Type_02;
	@Column(name=COL_I_TYPE_03, nullable = true)
	private	Integer         I_Type_03;
	
	@Column(name=COL_T_PROPERTY_01, nullable = true)
	private	String          T_Property_01;

	@Column(name=COL_T_PROPERTY_02, nullable = true)
	private	String          T_Property_02;

	@Column(name=COL_T_PROPERTY_03, nullable = true)
	private	String          T_Property_03;

	@Column(name=COL_T_PROPERTY_04, nullable = true)
	private	String          T_Property_04;
	//-----------------------Transient Variables-------------------------

	@Transient
	private List<TaTpyDocument>			O_Documents;
	
	@Transient
	private TaAutUser					O_User;
	
	@Transient
	private String						O_Login;
	
//	@Transient
//	private List<TaTpyTranslation> 		O_Translations;
	
	@Transient
	private List<TaNsoPost> 		    O_Posts;
	
	@Transient
	private Integer						O_View;
	
	@Transient
	private List<TaNsoPost>				O_Childs;
	
	@Transient
	private List<TaTpyCategoryEntity>	O_Cats;

	//---------------------Constructeurs-----------------------
	public TaNsoPost(){}

	public TaNsoPost(Map<String, Object> attrs) throws Exception {
		this.reqSetAttrFromMap(attrs);		

		if (this.F_Eval_01==null || this.F_Eval_01<EVAL_MIN) this.F_Eval_01 = EVAL_MIN;
		if (this.F_Eval_01>EVAL_MAX) this.F_Eval_01 = EVAL_MAX;

		if (this.F_Eval_02==null || this.F_Eval_02<EVAL_MIN) this.F_Eval_02 = EVAL_MIN;
		if (this.F_Eval_02>EVAL_MAX) this.F_Eval_02 = EVAL_MAX;

		if (this.F_Eval_03==null || this.F_Eval_03<EVAL_MIN) this.F_Eval_03 = EVAL_MIN;
		if (this.F_Eval_03>EVAL_MAX) this.F_Eval_03 = EVAL_MAX;

		if (this.F_Eval_04==null || this.F_Eval_04<EVAL_MIN) this.F_Eval_04 = EVAL_MIN;
		if (this.F_Eval_04>EVAL_MAX) this.F_Eval_04 = EVAL_MAX;



		this.F_Eval_01              = Math.ceil(this.F_Eval_01);
		this.F_Eval_02              = Math.ceil(this.F_Eval_02);
		this.F_Eval_03              = Math.ceil(this.F_Eval_03);
		this.F_Eval_04              = Math.ceil(this.F_Eval_04);


		//doInitDBFlag();
	}





	//---------------------EntityInterface-----------------------
	@Override
	public Serializable reqRef() {
		return this.I_ID;

	}

	@Override
	public void doMergeWith(TaNsoPost ent) {
		if (ent == this) return;
		this.F_Eval_01              = ent.F_Eval_01;
		this.F_Eval_02              = ent.F_Eval_02;
		this.F_Eval_03              = ent.F_Eval_03;
		this.F_Eval_04              = ent.F_Eval_04;
		this.F_Eval_05              = ent.F_Eval_05;
		this.T_Ref                  = ent.T_Ref;
		this.T_Title                = ent.T_Title;
		this.T_Content_01           = ent.T_Content_01;
		this.T_Content_02           = ent.T_Content_02;
		this.T_Content_03           = ent.T_Content_03;
		this.T_Content_04           = ent.T_Content_04;
		this.T_Content_05           = ent.T_Content_05;
		this.T_Content_06           = ent.T_Content_06;
		this.T_Content_07           = ent.T_Content_07;
		this.T_Content_08           = ent.T_Content_08;
		this.T_Content_09           = ent.T_Content_09;
		this.T_Content_10           = ent.T_Content_10;
		this.I_Type_03              = ent.I_Type_03;
		this.I_Status               = ent.I_Status;
		this.T_Comment              = ent.T_Comment;
		this.D_Date_New             = ent.D_Date_New;
		this.D_Date_Mod             = ent.D_Date_Mod;
		this.I_Aut_User_New         = ent.I_Aut_User_New;
		this.I_Aut_User_Mod         = ent.I_Aut_User_Mod;
		this.I_Nb_Resp              = ent.I_Nb_Resp;
		this.I_Parent               = ent.I_Parent;
		this.I_Entity_Type          = ent.I_Entity_Type;
		this.I_Entity_ID            = ent.I_Entity_ID;
		this.I_Type_01            	= ent.I_Type_01;
		this.I_Type_02            	= ent.I_Type_02;



		//---------------------Merge Transient Variables if exist-----------------------
	}

	@Override
	public boolean equals(Object o)  {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		boolean ok = false;

		ok = (I_ID == ((TaNsoPost)o).I_ID);
		if (!ok) return ok;


		if (!ok) return ok;
		return ok;
	}

	@Override
	public int hashCode() {
		return this.I_ID;

	}

	@Override
	public String toString() {
		return 	"TaNsoPost { " +
				"I_ID:"+                      I_ID +"," + 
				"F_Eval_01:"+                 F_Eval_01 +"," + 
				"F_Eval_02:"+                 F_Eval_02 +"," + 
				"F_Eval_03:"+                 F_Eval_03 +"," + 
				"F_Eval_04:"+                 F_Eval_04 +"," + 
				"F_Eval_05:"+                 F_Eval_05 +"," + 
				"T_Ref:"+                     T_Ref +"," + 
				"T_Title:"+                   T_Title +"," + 
				"T_Content_01:"+              T_Content_01 +"," + 
				"T_Content_02:"+              T_Content_02 +"," + 
				"T_Content_03:"+              T_Content_03 +"," + 
				"T_Content_04:"+              T_Content_04 +"," + 
				"T_Content_05:"+              T_Content_05 +"," + 
				"T_Content_06:"+              T_Content_06 +"," + 
				"T_Content_07:"+              T_Content_07 +"," + 
				"T_Content_08:"+              T_Content_08 +"," + 
				"T_Content_09:"+              T_Content_09 +"," + 
				"T_Content_10:"+              T_Content_10 +"," + 
				"I_Type:"+                    I_Type_03 +"," + 
				"I_Status:"+                  I_Status +"," + 
				"T_Comment:"+                 T_Comment +"," + 
				"D_Date_Add:"+                D_Date_New +"," + 
				"D_Date_Mod:"+                D_Date_Mod +"," + 
				"I_Aut_User:"+                I_Aut_User_New +"," + 
				"I_Nb_Resp:"+                 I_Nb_Resp +"," + 
				"I_Parent:"+                  I_Parent +"," + 
				"I_Entity_Type:"+             I_Entity_Type +"," + 
				"I_Entity_ID:"+               I_Entity_ID +"," +
				"I_Type_01:"+                 I_Type_01 +"," + 
				"I_Type_02:"+              	  I_Type_02 +"," + 
				"}";

	}

	//--------------------------------------------------------------------------
	@Transient
	private boolean isBuilt_public 	= false;
	@Transient
	private boolean isBuilt_manager = false;

	public void doBuildAll(boolean forced, boolean forManager) throws Exception {
		doBuildDocuments(forced);
//		doBuildTranslations(forced);
		doBuildUserLogin(forced);
//		doBuildCats(forced);
		
		if (forManager && !isBuilt_manager) {
			isBuilt_manager=true;
		}

//		if (forManager && this.I_Type_03.equals(POST_TYPE_BLOG)) {
//			doBuildPosts (false);
//		}
	}

	public void doBuildDocuments(boolean forced) throws Exception{
		if (!forced && (this.O_Documents!=null)){
			return;
		}

		//		O_Documents = TaTpyDocument.DAO.reqList(
		//				Restrictions.eq(TaTpyDocument.ATT_I_PARENT_TYPE	, DBConfig.ID_TA_NSO_POST), 
		//				Restrictions.eq(TaTpyDocument.ATT_I_PARENT_ID	, I_ID)
		//		);		
		O_Documents = TaTpyDocument.reqTpyDocuments(DefDBExt.ID_TA_NSO_POST, I_ID, null, null);

		if (O_Documents!=null) {
			for (TaTpyDocument doc : O_Documents) {
				doc.reqSet(TaTpyDocument.ATT_T_INFO_02 , null); //hide to client
			}
		}
	}

	public void doBuildUser(boolean forced) throws Exception{
		if (!forced && (this.O_User!=null)){
			return;
		}

		if(I_Aut_User_New != null && I_Aut_User_New > 0) {
			//			O_User = TaAutUser.DAO.reqEntityByRef(I_Aut_User);
			//			O_User.doBuildPerson(forced, false);
			O_User = TaAutUser.DAO.reqEntityByRef(I_Aut_User_New);
		}
	}

	public void doBuildUserLogin(boolean forced) throws Exception{
		if(this.I_Aut_User_New == null || this.I_Aut_User_New == 0) return;
		if (!forced && this.O_Login != null ) return;
		
		this.O_Login = "";
		if(this.I_Aut_User_New == 0) {
			this.O_Login = "--";
		} else {
			TaAutUser u = TaAutUser.DAO.reqEntityByRef(I_Aut_User_New);
			if(u!= null) {
				String login = (String) u.req(TaAutUser.ATT_T_LOGIN_01);
				if(login != null) this.O_Login = login;
			}
		}
	}
	
//	public void doBuildCats(boolean forced) throws Exception {
//		if (!forced && O_Cats!=null) return;
//		
//		this.O_Cats = TaTpyCategoryEntity.DAO.reqList(
//				Restrictions.eq(TaTpyCategoryEntity.ATT_I_ENTITY_TYPE	, DBConfig.ID_TA_NSO_TPY_CAT_BLOG), 
//				Restrictions.eq(TaTpyCategoryEntity.ATT_I_ENTITY_ID		, this.I_ID)
//		);
//	}
//
//	private void doBuildPosts(boolean forced) throws Exception {		
//		if(!forced && this.O_Posts!=null) {
//			return;
//		}
//
//		List<TaNsoPost> taLst		= TaNsoPost.DAO.reqList(0,10,
//				Restrictions.eq(TaNsoPost.ATT_I_ENTITY_TYPE	, DBConfig.ID_TA_NSO_POST),
//				Restrictions.eq(TaNsoPost.ATT_I_ENTITY_ID, this.I_ID));
//
//		for (TaNsoPost p : taLst) {
//			p.doBuildAll(forced, false);
//		}
//
//		this.O_Posts = taLst;
//	}


	public void doCalculateEvalTotal(int nbEval) throws Exception {
		if (F_Eval_05!=null) return;

		if (this.F_Eval_01==null || this.F_Eval_01<EVAL_MIN) this.F_Eval_01 = EVAL_MIN;
		if (this.F_Eval_01>EVAL_MAX) this.F_Eval_01 = EVAL_MAX;

		if (this.F_Eval_02==null || this.F_Eval_02<EVAL_MIN) this.F_Eval_02 = EVAL_MIN;
		if (this.F_Eval_02>EVAL_MAX) this.F_Eval_02 = EVAL_MAX;

		if (this.F_Eval_03==null || this.F_Eval_03<EVAL_MIN) this.F_Eval_03 = EVAL_MIN;
		if (this.F_Eval_03>EVAL_MAX) this.F_Eval_03 = EVAL_MAX;

		if (this.F_Eval_04==null || this.F_Eval_04<EVAL_MIN) this.F_Eval_04 = EVAL_MIN;
		if (this.F_Eval_04>EVAL_MAX) this.F_Eval_04 = EVAL_MAX;


		switch(nbEval) {
		case 1: F_Eval_05 = F_Eval_01;									break;
		case 2: F_Eval_05 = F_Eval_01+F_Eval_02;						break;
		case 3: F_Eval_05 = F_Eval_01+F_Eval_02+F_Eval_03;				break;
		case 4: F_Eval_05 = F_Eval_01+F_Eval_02+F_Eval_03+F_Eval_04;	break;
		}	

		F_Eval_05 = (F_Eval_05/nbEval);		
		if (F_Eval_05>EVAL_MAX) F_Eval_05= EVAL_MAX;
		if (F_Eval_05<EVAL_MIN) F_Eval_05= EVAL_MIN;
	}

	public static List<TaNsoPost> reqNsoPostLstThreadEvaluation(Date begin, Date end) throws Exception {
		List<TaNsoPost> 	list 	= TaNsoPost.DAO.reqList(Restrictions.between(TaNsoPost.ATT_D_DATE_MOD, begin, end));
		return list;
	}


	public void doCheckVal(int nbEval) {
		if (F_Eval_05!=null) return;

		if (this.F_Eval_01==null || this.F_Eval_01<EVAL_MIN) this.F_Eval_01 = EVAL_MIN;
		if (this.F_Eval_01>EVAL_MAX) this.F_Eval_01 = EVAL_MAX;

		if (this.F_Eval_02==null || this.F_Eval_02<EVAL_MIN) this.F_Eval_02 = EVAL_MIN;
		if (this.F_Eval_02>EVAL_MAX) this.F_Eval_02 = EVAL_MAX;

		if (this.F_Eval_03==null || this.F_Eval_03<EVAL_MIN) this.F_Eval_03 = EVAL_MIN;
		if (this.F_Eval_03>EVAL_MAX) this.F_Eval_03 = EVAL_MAX;

		if (this.F_Eval_04==null || this.F_Eval_04<EVAL_MIN) this.F_Eval_04 = EVAL_MIN;
		if (this.F_Eval_04>EVAL_MAX) this.F_Eval_04 = EVAL_MAX;

		//		if (this.F_Eval_05==null || this.F_Eval_05<EVAL_MIN) this.F_Eval_05 = EVAL_MIN;
		//		if (this.F_Eval_05>EVAL_MAX) this.F_Eval_05 = EVAL_MAX;

		this.F_Eval_01              = Math.ceil(this.F_Eval_01);
		this.F_Eval_02              = Math.ceil(this.F_Eval_02);
		this.F_Eval_03              = Math.ceil(this.F_Eval_03);
		this.F_Eval_04              = Math.ceil(this.F_Eval_04);
		//		this.F_Eval_05              = Math.ceil(this.F_Eval_05);

		switch(nbEval) {
		case 1: F_Eval_05 = F_Eval_01;									break;
		case 2: F_Eval_05 = F_Eval_01+F_Eval_02;						break;
		case 3: F_Eval_05 = F_Eval_01+F_Eval_02+F_Eval_03;				break;
		case 4: F_Eval_05 = F_Eval_01+F_Eval_02+F_Eval_03+F_Eval_04;	break;
		}	

		//		F_Eval_05 = Math.ceil(F_Eval_05/nbEval);
		F_Eval_05 = F_Eval_05/nbEval;

		if (F_Eval_05>EVAL_MAX) F_Eval_05= EVAL_MAX;
		if (F_Eval_05<EVAL_MIN) F_Eval_05= EVAL_MIN;
	}

	public double reqEval(int index) {

		switch(index) {
		case 0: return F_Eval_01!=null? F_Eval_01 : EVAL_MIN;
		case 1: return F_Eval_02!=null? F_Eval_02 : EVAL_MIN;
		case 2: return F_Eval_03!=null? F_Eval_03 : EVAL_MIN;
		case 3: return F_Eval_04!=null? F_Eval_04 : EVAL_MIN;
		case 4: return F_Eval_05!=null? F_Eval_05 : EVAL_MIN;
		}
		return 0;
	}

//	public static void doBuildListTranslations(List<TaNsoPost> list) throws Exception{
//		TranslationTool.doBuildTpyTranslations(list, DBConfig.ID_TA_NSO_POST, ViNsoPost.ATT_O_TRANSLATIONS, false);
//	}
//	
	public void doAddDocument(List<TaTpyDocument> docs) {
		if (docs==null || docs.size()==0) return;
		if (this.O_Documents== null) O_Documents =  new ArrayList<TaTpyDocument> ();
		O_Documents.addAll(docs);
	}
}
