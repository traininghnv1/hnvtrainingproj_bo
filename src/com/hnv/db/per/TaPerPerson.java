package com.hnv.db.per;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hnv.api.def.DefDBExt;
import com.hnv.api.main.API;
import com.hnv.api.main.Hnv_CfgHibernate;
import com.hnv.common.tool.ToolDBEntity;
import com.hnv.common.tool.ToolJSON;
import com.hnv.data.json.JSONObject;
import com.hnv.db.EntityAbstract;
import com.hnv.db.EntityDAO;
import com.hnv.db.tpy.TaTpyDocument;
import com.hnv.db.tpy.TaTpyInformation;

/**
 * TaPerPerson by H&V SAS
 */
@Entity
@Table(name = DefDBExt.TA_PER_PERSON )
public class TaPerPerson extends EntityAbstract<TaPerPerson> {
	public  static final int 	TYP_01_MORAL			= 1000001;
	public  static final int 	TYP_01_NATURAL			= 1000002;
	
	public  static final int 	TYP_02_AGENT			= 1010001;
	public  static final int 	TYP_02_CLIENT			= 1010002;
	public  static final int 	TYP_02_SUPPLIER			= 1010003;
	public  static final int 	TYP_02_PRODUCER			= 1010004;	
	public  static final int 	TYP_02_DOCTOR			= 1010005;
	public  static final int 	TYP_02_TPARTY			= 1010006;
	public  static final int 	TYP_02_PROSPECT			= 1010007;
	public  static final int 	TYP_02_CLIENT_PUBLIC	= 1010008;

	public  static final int 	TYP_02_COMPANY			= 1010010;
	public  static final int 	TYP_02_BRANCH			= 1010011;
	public  static final int 	TYP_02_DEPARTMENT		= 1010012;
	
	public  static final int 	STAT_WAITING		= 0; //moi tao	
	public  static final int 	STAT_ACTIVE_LV_01	= 1; //call, email only	
	public  static final int 	STAT_ACTIVE_LV_02	= 2; //sign the first paper..
	public  static final int 	STAT_ACTIVE_LV_03	= 3; //high coperation with contract
	
	public  static final int 	STAT_INACTIVE		= 10;
	public  static final int 	STAT_REPORTED		= 11;
	
	public  static final int 	STAT_DELETED		= 100;
	
	//------------------------------------------------------------------------------
	private static final long 	serialVersionUID = 1L;

	//---------------------------List of Column from DB-----------------------------
	public static final String	COL_I_ID                              =	"I_ID";
	public static final String	COL_I_STATUS                          =	"I_Status";
	public static final String	COL_I_TYPE_01                         =	"I_Type_01";//cfgGroup 100 - 1: pN, 2: pM, 3: pM Divison/Departement
	public static final String	COL_I_TYPE_02                         =	"I_Type_02";//cfgGroup 101 - 0: agent 1: client, 2: supplier, 3: produce
	public static final String	COL_T_CODE_01                         =	"T_Code_01";
	public static final String	COL_T_CODE_02                         =	"T_Code_02";
	public static final String	COL_T_NAME_01                         =	"T_Name_01";
	public static final String	COL_T_NAME_02                         =	"T_Name_02";
	public static final String	COL_T_NAME_03                         =	"T_Name_03";
	public static final String	COL_T_INFO_01                         =	"T_Info_01"; 	//--ID, Mã quản lý intern
	public static final String	COL_T_INFO_02                         =	"T_Info_02"; 	//--Mã số thuế, Tax Code, 
	public static final String	COL_T_INFO_03                         =	"T_Info_03"; 	//--commentaire
	public static final String	COL_T_INFO_04                         =	"T_Info_04";	//--paper, date, location
	public static final String	COL_T_INFO_05                         =	"T_Info_05";
	public static final String	COL_I_AUT_USER_01                     =	"I_Aut_User_01";
	public static final String	COL_I_AUT_USER_02                     =	"I_Aut_User_02";
	public static final String	COL_D_DATE_01                         =	"D_Date_01";
	public static final String	COL_D_DATE_02                         =	"D_Date_02";
	public static final String	COL_D_DATE_03                   	  =	"D_Date_03";
	
	public static final String	COL_I_PER_MANAGER                     =	"I_Per_Manager";


	//---------------------------List of ATTR of class-----------------------------
	public static final String	ATT_I_ID                              =	"I_ID";
	public static final String	ATT_I_STATUS                          =	"I_Status";
	public static final String	ATT_I_TYPE_01                         =	"I_Type_01";//cfgGroup 100 - 1: pN, 2: pM, 3: pM Divison/Departement
	public static final String	ATT_I_TYPE_02                         =	"I_Type_02";//cfgGroup 101 - 0: agent 1: client, 2: supplier, 3: produce
	public static final String	ATT_T_CODE_01                         =	"T_Code_01";
	public static final String	ATT_T_CODE_02                         =	"T_Code_02";
	public static final String	ATT_T_NAME_01                         =	"T_Name_01";
	public static final String	ATT_T_NAME_02                         =	"T_Name_02";
	public static final String	ATT_T_NAME_03                         =	"T_Name_03";
	public static final String	ATT_T_INFO_01                         =	"T_Info_01"; 	//--ID, Mã quản lý intern
	public static final String	ATT_T_INFO_02                         =	"T_Info_02"; 	//--Mã số thuế, Tax Code, 
	public static final String	ATT_T_INFO_03                         =	"T_Info_03"; 	//--commentaire
	public static final String	ATT_T_INFO_04                         =	"T_Info_04";	//--paper, date, location
	public static final String	ATT_T_INFO_05                         =	"T_Info_05";
	public static final String	ATT_I_AUT_USER_01                     =	"I_Aut_User_01";
	public static final String	ATT_I_AUT_USER_02                     =	"I_Aut_User_02";
	public static final String	ATT_D_DATE_01                         =	"D_Date_01";
	public static final String	ATT_D_DATE_02                         =	"D_Date_02";
	public static final String	ATT_D_DATE_03                         =	"D_Date_03";
	public static final String	ATT_I_PER_MANAGER                     =	"I_Per_Manager";
	
	public static final String	ATT_O_AVATAR               	  	  	  =	"O_Avatar";
	
	public static final String	ATT_O_PROPERTIES                   	  =	"O_Properties";
	public static final String	ATT_O_INFORMATIONS                	  =	"O_Informations";
	public static final String	ATT_O_DOCUMENTS                	  	  =	"O_Documents";
	public static final String	ATT_O_POSITIONS                	  	  =	"O_Positions";
	public static final String	ATT_O_ACCOUNT_ENT                     =	"O_Account_Ent";
	public static final String	ATT_O_FULLNAME	                      =	"O_Fullname";
	
	public static final String	ATT_O_SHORT_ADDR	                  =	"O_Short_Addr";
	public static final String	ATT_O_LONG_ADDR	                      =	"O_Long_Addr";

	//-------every entity class must initialize its DAO from here -----------------------------
	private 	static 	final boolean[] 			RIGHTS		= {true, true, true, true, false}; //canRead, canAdd, canUpd, canDel, del physique or flag only 
	private 	static	final boolean				API_CACHE 	= false;
	private 	static 	final boolean[]				HISTORY		= {false, false, false}; //add, mod, del

	public		static 	final EntityDAO<TaPerPerson> 	DAO;
	static{
		DAO = new EntityDAO<TaPerPerson>(Hnv_CfgHibernate.reqFactoryEMSession(Hnv_CfgHibernate.ID_FACT_MAIN) , TaPerPerson.class,RIGHTS, HISTORY, DefDBExt.TA_PER_PERSON, DefDBExt.ID_TA_PER_PERSON);

		/*	
		DAO.doSetIndexer(APIConfig.API_INDEXER, 
				ATT_I_ID,  //obligatoire pour lineID, les suivants pour composer le content pour l'indexation
//				ATT_ ..., 

				//next level with list children				
//				EntityIndexTool		.IND_ENT_LST_BEGIN,
//					TaPerPerson.ATT_O_, 
//						TaPerPerson_Child	.ATT_T__01,
//						TaPerPerson_Child	.ATT_T__02,				
//				EntityIndexTool		.IND_ENT_LST_END


				//next level with object single
//				EntityIndexTool.IND_ENT_SING_BEGIN
//					TaPerPerson.ATT_O_, 
//						TaPerPerson_Child	.ATT_T__01,
//						TaPerPerson_Child	.ATT_T__02,	
//				EntityIndexTool.IND_ENT_SING_END
		);
		 */

	}

	//-----------------------Class Attributs-------------------------
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name=COL_I_ID, nullable = false)
	private	Integer         I_ID;

	@Column(name=COL_I_STATUS, nullable = false)
	private	Integer         I_Status;

	@Column(name=COL_I_TYPE_01, nullable = true)
	private	Integer         I_Type_01;

	@Column(name=COL_I_TYPE_02, nullable = true)
	private	Integer         I_Type_02;

	@Column(name=COL_D_DATE_01, nullable = true)
	private	Date         D_Date_01;

	@Column(name=COL_D_DATE_02, nullable = true)
	private	Date         D_Date_02;
	
	@Column(name=COL_D_DATE_03, nullable = true)
	private	Date         D_Date_03;
	
	@Column(name=COL_T_CODE_01, nullable = true)
	private	String          T_Code_01;
	
	@Column(name=COL_T_CODE_02, nullable = true)
	private	String          T_Code_02;

	@Column(name=COL_T_NAME_01, nullable = true)
	private	String          T_Name_01;

	@Column(name=COL_T_NAME_02, nullable = true)
	private	String          T_Name_02;

	@Column(name=COL_T_NAME_03, nullable = true)
	private	String          T_Name_03;
	
	@Column(name=COL_T_INFO_01, nullable = true)
	private	String          T_Info_01;

	@Column(name=COL_T_INFO_02, nullable = true)
	private	String          T_Info_02;

	@Column(name=COL_T_INFO_03, nullable = true)
	private	String          T_Info_03;

	@Column(name=COL_T_INFO_04, nullable = true)
	private	String          T_Info_04;

	@Column(name=COL_T_INFO_05, nullable = true)
	private	String          T_Info_05;

	@Column(name=COL_I_AUT_USER_01, nullable = true)
	private Integer			I_Aut_User_01;
	
	@Column(name=COL_I_AUT_USER_02, nullable = true)
	private Integer			I_Aut_User_02;
	
	@Column(name=COL_I_PER_MANAGER, nullable = true)
	private	Integer         I_Per_Manager;


	//-----------------------Transient Variables-------------------------

	@Transient
	private	List<TaTpyDocument> 		O_Documents;

	@Transient
	private	List<TaTpyInformation> 		O_Informations;
	
	@Transient
	private	String 						O_FullName;
	
	@Transient
	private	TaTpyDocument 				O_Avatar;
	
	//---------------------Constructeurs-----------------------
	private TaPerPerson(){}

	public TaPerPerson(Map<String, Object> attrs) throws Exception {
		this.reqSetAttrFromMap(attrs);
		//doInitDBFlag();
	}

	public TaPerPerson(Integer id, String name01, String name02) throws Exception {
		this.reqSetAttr(
				ATT_I_ID		  , id,
				ATT_T_NAME_01     , name01,
				ATT_T_NAME_01     , name02				
				);
		//doInitDBFlag();
	}
	
	public TaPerPerson(Integer I_Status) throws Exception {
		this.reqSetAttr(
				ATT_I_STATUS     , I_Status
				);
		//doInitDBFlag();
	}

	//---------------------EntityInterface-----------------------
	@Override
	public Serializable reqRef() {
		return this.I_ID;

	}

	@Override
	public void doMergeWith(TaPerPerson ent) {
		if (ent == this) return;
		this.I_Status               = ent.I_Status;
		this.I_Type_01              = ent.I_Type_01;
		this.I_Type_02              = ent.I_Type_02;
		this.T_Code_01              = ent.T_Code_01;
		this.T_Name_01              = ent.T_Name_01;
		this.T_Name_02              = ent.T_Name_02;
		this.T_Name_03              = ent.T_Name_03;
		this.T_Info_01              = ent.T_Info_01;
		this.T_Info_02              = ent.T_Info_02;
		this.T_Info_03              = ent.T_Info_03;
		this.T_Info_04              = ent.T_Info_04;
		this.T_Info_05              = ent.T_Info_05;
		this.D_Date_01             	= ent.D_Date_01;
		this.D_Date_02             	= ent.D_Date_02;
		this.D_Date_03        		= ent.D_Date_03;
		this.I_Aut_User_01			= ent.I_Aut_User_01;
		this.I_Aut_User_02    		= ent.I_Aut_User_02;
		//---------------------Merge Transient Variables if exist-----------------------
	}

	@Override
	public boolean equals(Object o)  {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		boolean ok = false;

		ok = (I_ID == ((TaPerPerson)o).I_ID);
		if (!ok) return ok;


		if (!ok) return ok;
		return ok;
	}

	@Override
	public int hashCode() {
		return this.I_ID;

	}

	//----------------------------------------------------------------------------------------------------------	
	public Integer reqID(){
		return this.I_ID;
	}
	//----------------------------------------------------------------------------------------------------------		
	public void doBuildDocuments (boolean forced) throws Exception{
		if (!forced && this.O_Documents!=null) return;		
		this.O_Documents = TaTpyDocument.DAO.reqList(Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_TYPE, DefDBExt.ID_TA_PER_PERSON), Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_ID, I_ID));	
	}
	
	public void doRemoveDocuments () throws Exception{				
		this.doBuildDocuments(true);		
		if (O_Documents!=null && O_Documents.size()>0)
			TaTpyDocument.DAO.doRemove(O_Documents);	
		O_Documents = null;
	}

	public void doRemoveDocuments (Collection docIds) throws Exception{				
		List<TaTpyDocument> lP  = TaTpyDocument.DAO.reqList(Restrictions.in(TaTpyDocument.ATT_I_ID, docIds));
		if (lP!=null && lP.size()>0) TaTpyDocument.DAO.doRemove(lP);

		this.doBuildDocuments(true);
	}

	public void doRemoveDocuments (Integer... docIds) throws Exception{		
		List<TaTpyDocument> lP  = TaTpyDocument.DAO.reqList(Restrictions.in(TaTpyDocument.ATT_I_ID, docIds));
		if (lP!=null && lP.size()>0) TaTpyDocument.DAO.doRemove(lP);

		this.doBuildDocuments(true);
	}

	//----------------------------------------------------------------------------------------------------------	
	//----------------------------------------------------------------------------------------------------------
	public static void doBuildInformations (List<TaPerPerson> lst) throws Exception{
		for (TaPerPerson per : lst) {
			per.O_Informations = new ArrayList<TaTpyInformation>();
		}
		
		Hashtable<Integer, EntityAbstract> dict = ToolDBEntity.reqTabKeyInt(lst, TaPerPerson.ATT_I_ID);
		List<TaTpyInformation> infos = TaTpyInformation.DAO.reqList_In ( TaTpyInformation.ATT_I_ENTITY_ID, dict.keySet(), Restrictions.eq(TaTpyInformation.ATT_I_ENTITY_TYPE, DefDBExt.ID_TA_PER_PERSON));
		for (TaTpyInformation inf : infos) {
			Integer pId = (Integer) inf.req(TaTpyInformation.ATT_I_ENTITY_ID);
			TaPerPerson per = (TaPerPerson)dict.get(pId);
			if (per!=null) {
				per.O_Informations.add(inf);
			}
		}
	}
	public void doBuildInformations (boolean forced) throws Exception{
		if (!forced && this.O_Informations!=null) return;		
		//same base
		this.O_Informations = TaTpyInformation.DAO.reqList(Order.desc(TaTpyInformation.ATT_I_ID), Restrictions.eq(TaTpyInformation.ATT_I_ENTITY_TYPE, DefDBExt.ID_TA_PER_PERSON), Restrictions.eq(TaTpyInformation.ATT_I_ENTITY_ID, I_ID));
	}
	public void doRemoveInformations () throws Exception{				
		this.doBuildInformations(true);		
		if (O_Informations!=null && O_Informations.size()>0)
			TaTpyInformation.DAO.doRemove(O_Informations);		
		O_Informations = null;
	}

	public void doRemoveInformations (Collection infIds) throws Exception{				
		List<TaTpyInformation> lP  = TaTpyInformation.DAO.reqList(Restrictions.in(TaTpyDocument.ATT_I_ID, infIds));
		if (lP!=null && lP.size()>0) TaTpyInformation.DAO.doRemove(lP);

		this.doBuildInformations(true);
	}

	public void doRemoveInformations (Integer... infIds) throws Exception{				
		List<TaTpyInformation> lP  = TaTpyInformation.DAO.reqList(Restrictions.in(TaTpyDocument.ATT_I_ID, infIds));
		if (lP!=null && lP.size()>0) TaTpyInformation.DAO.doRemove(lP);

		this.doBuildInformations(true);
	}
	//----------------------------------------------------------------------------------------------------------	
	//----------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------	
	//----------------------------------------------------------------------------------------------------------
	

	public void doBuildFullName(boolean forced) throws Exception{
		if (!forced && O_FullName!=null) return;
		String fullName = "";
		if(T_Name_01 != null) fullName += T_Name_01 + " ";
		if(T_Name_02 != null) fullName += T_Name_02 + " ";
		this.O_FullName = fullName;
	}
	
	public void doBuildFiles(boolean forced) throws Exception {
		if (!forced && O_Documents!=null) return;

		this.O_Documents = TaTpyDocument.DAO.reqList(
				Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_TYPE	, DefDBExt.ID_TA_PER_PERSON),
				Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_ID	, this.I_ID));	
		
		for (TaTpyDocument d : O_Documents) {
			d.reqSet(TaTpyDocument.ATT_T_INFO_03, null);
			d.reqSet(TaTpyDocument.ATT_T_INFO_04, null);
		}
	}
	
	public void doBuildFiles(boolean forced, Integer typ01, Integer typ02) throws Exception {
		if (!forced && O_Documents!=null) return;

		this.O_Documents = TaTpyDocument.DAO.reqList(
				Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_TYPE	, DefDBExt.ID_TA_PER_PERSON),
				Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_ID	, this.I_ID),
				Restrictions.eq(TaTpyDocument.ATT_I_TYPE_01		, typ01),
				Restrictions.eq(TaTpyDocument.ATT_I_TYPE_02		, typ02));	
		
		for (TaTpyDocument d : O_Documents) {
			d.reqSet(TaTpyDocument.ATT_T_INFO_03, null);
			d.reqSet(TaTpyDocument.ATT_T_INFO_04, null);
		}
	}
	
	
	
	
	//-------------------------------------------------------------------------------------------------

	public static List<TaPerPerson> reqListPerson(Integer manId, String searchkey, Set<Integer>  type01, Set<Integer> type02, Integer nbLine) throws Exception {
		Criterion cri = null;		
		cri 			= Restrictions.ilike(TaPerPerson.ATT_T_NAME_01						, "%"+searchkey+"%");
		cri 			= Restrictions.or(cri, Restrictions.ilike(TaPerPerson.ATT_T_NAME_02	, "%"+searchkey+"%"));

		if(type01 != null)
			cri 	= Restrictions.and(cri, Restrictions.in(TaPerPerson.ATT_I_TYPE_01, type01));
		
		if(type02 != null)
			cri 	= Restrictions.and(cri, Restrictions.in(TaPerPerson.ATT_I_TYPE_02, type02)); 

//		if(stats != null && stats.length() > 0)
//			cri 	= Restrictions.and(cri, Restrictions.in(TaPerPerson.ATT_I_STATUS, stats));

		
		if(manId != null)
			cri 	= Restrictions.and(cri, Restrictions.eq(TaPerPerson.ATT_I_PER_MANAGER, manId));

		List<TaPerPerson> lstResult = TaPerPerson.DAO.reqList(0, nbLine, cri);
		return lstResult;
	}


	public static TaPerPerson reqNewCustomerForOrder(Session sess, int orderId, Integer manId, String customerStr) throws Exception {
		JSONObject customerObj	= (JSONObject) ToolJSON.reqJSonObjectFromString(customerStr);
		if (customerObj!=null) {
			Map<String, Object> info 	= API.reqMapParamsByClass(customerObj, TaPerPerson.class);
			TaPerPerson customer 		= new TaPerPerson(info);
			customer.reqSet(TaPerPerson.ATT_I_ID		, null);
			customer.reqSet(TaPerPerson.ATT_D_DATE_01	, new Date());
			customer.reqSet(TaPerPerson.ATT_I_PER_MANAGER	, manId);
			customer.reqSet(TaPerPerson.ATT_I_STATUS    , TaPerPerson.STAT_ACTIVE_LV_01);
			customer.reqSet(TaPerPerson.ATT_I_TYPE_01   , TaPerPerson.TYP_01_NATURAL);
			customer.reqSet(TaPerPerson.ATT_I_TYPE_02   , TaPerPerson.TYP_02_CLIENT);
			TaPerPerson.DAO.doPersist(sess, customer);
			
			Integer customerId = customer.reqID();
			customer.reqSet(TaPerPerson.COL_T_CODE_01  , "CST-" + manId.toString() + "-" + customerId.toString() );
			TaPerPerson.DAO.doMerge(sess, customer);
			return customer;
		}
		return null;
	}
	
	public static TaPerPerson reqNewCustomerForOrder(Session sess, int orderId, Integer manId, JSONObject customerObj) throws Exception {
		if (customerObj!=null) {
			Map<String, Object> info 	= API.reqMapParamsByClass(customerObj, TaPerPerson.class);
			TaPerPerson customer 	= new TaPerPerson(info);
			customer.reqSet(TaPerPerson.ATT_D_DATE_01	, new Date());
			customer.reqSet(TaPerPerson.ATT_I_PER_MANAGER	, manId);
			customer.reqSet(TaPerPerson.ATT_I_STATUS    , TaPerPerson.STAT_ACTIVE_LV_01);
			customer.reqSet(TaPerPerson.ATT_I_TYPE_01   , TaPerPerson.TYP_01_NATURAL);
			customer.reqSet(TaPerPerson.ATT_I_TYPE_02   , TaPerPerson.TYP_02_CLIENT);
			TaPerPerson.DAO.doPersist(sess, customer);
			Integer customerId = customer.reqID();
			customer.reqSet(TaPerPerson.COL_T_CODE_01  , "CST-" + manId.toString() + "-" + customerId.toString() );
			TaPerPerson.DAO.doMerge(sess, customer);
			return customer;
		}
		return null;
	}
}
