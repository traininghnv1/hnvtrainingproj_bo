package com.hnv.db.aut;

//import java.lang.String;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hnv.api.def.DefDBExt;
import com.hnv.api.main.Hnv_CfgHibernate;
import com.hnv.common.tool.ToolSet;
import com.hnv.db.EntityAbstract;
import com.hnv.db.EntityDAO;
import com.hnv.db.per.TaPerPerson;
import com.hnv.db.tpy.TaTpyDocument;

@Entity
@Table(name = DefDBExt.TA_AUT_USER )
public class TaAutUser extends EntityAbstract<TaAutUser> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int 	TYPE_01_ADM_ALL		= 100;
	public static final int 	TYPE_01_ADM			= 1;
	public static final int 	TYPE_01_AGENT		= 2;
	
	public static final int 	TYPE_01_MENTOR     	= 6;
	public static final int 	TYPE_01_CLIENT		= 4;
	
	public static final int 	TYPE_01_VISITOR		= 10;
	
	
	public static final int		STAT_INACTIVE 	= 0;
	public static final int		STAT_ACTIVE 	= 1;
	public static final int		STAT_WAITING 	= 2;
	public static final int		STAT_DELETED 	= 10;

	// ---------------------------List of Column from
	// DB-----------------------------
	public static final String COL_I_ID 			= "I_ID";
	public static final String COL_T_LOGIN_01		= "T_Login_01";
	public static final String COL_T_LOGIN_02		= "T_Login_02";
	
	public static final String COL_T_PASS_01		= "T_Pass_01";
	public static final String COL_T_PASS_02		= "T_Pass_02";
	
	public static final String COL_I_STATUS 		= "I_Status";
	
	public static final String COL_I_TYPE_01 		= "I_Type_01";
	public static final String COL_I_TYPE_02 		= "I_Type_02";
	
	public static final String COL_T_INFO_01 		= "T_Info_01"; //email
	public static final String COL_T_INFO_02 		= "T_Info_02"; //tel
	public static final String COL_T_INFO_03 		= "T_Info_03";
	public static final String COL_T_INFO_04 		= "T_Info_04";
	public static final String COL_T_INFO_05 		= "T_Info_05";
	
	public static final String COL_D_DATE_01 		= "D_Date_01"; //date new
	public static final String COL_D_DATE_02 		= "D_Date_02"; //date mod
	public static final String COL_D_DATE_03 		= "D_Date_03"; //date validation
	
	public static final String COL_I_AUT_USER_01 	= "I_Aut_User_01"; //user new
	public static final String COL_I_AUT_USER_02 	= "I_Aut_User_02"; //user mod
	public static final String COL_I_AUT_USER_03 	= "I_Aut_User_03"; //supervisor
	
	public static final String COL_I_PER_MANAGER 	= "I_Per_Manager";
	public static final String COL_I_PER_PERSON 	= "I_Per_Person";

	// ---------------------------List of ATTR of class-----------------------------
	public static final String ATT_I_ID 			= "I_ID";
	public static final String ATT_T_LOGIN_01		= "T_Login_01";
	public static final String ATT_T_LOGIN_02		= "T_Login_02";
	
	public static final String ATT_T_PASS_01		= "T_Pass_01";
	public static final String ATT_T_PASS_02		= "T_Pass_02";
	
	public static final String ATT_I_STATUS 		= "I_Status";
	
	public static final String ATT_I_TYPE_01 		= "I_Type_01";
	public static final String ATT_I_TYPE_02 		= "I_Type_02";
	
	public static final String ATT_D_DATE_01 		= "D_Date_01";
	public static final String ATT_D_DATE_02 		= "D_Date_02";
	public static final String ATT_D_DATE_03 		= "D_Date_03";
	public static final String ATT_T_INFO_01 		= "T_Info_01";
	public static final String ATT_T_INFO_02 		= "T_Info_02";
	public static final String ATT_T_INFO_03 		= "T_Info_03";
	public static final String ATT_T_INFO_04 		= "T_Info_04";
	public static final String ATT_T_INFO_05 		= "T_Info_05";
	public static final String ATT_I_AUT_USER_01 	= "I_Aut_User_01";
	public static final String ATT_I_AUT_USER_02 	= "I_Aut_User_02";
	public static final String ATT_I_AUT_USER_03 	= "I_Aut_User_03";
	public static final String ATT_I_PER_MANAGER 	= "I_Per_Manager";
	public static final String ATT_I_PER_PERSON 	= "I_Per_Person";

	public static final String ATT_O_ROLES 			= "O_Roles";
	public static final String ATT_O_RIGHTS 		= "O_Rights";
	
	public static final String ATT_O_PER_MANAGER 	= "O_Per_Manager";
	public static final String ATT_O_PER_PERSON     = "O_Per_Person";
	
	public static final String ATT_O_DOCUMENTS 		= "O_Documents";
	public static final String ATT_O_AVATAR	        = "O_Avatar";
//	public static final String ATT_O_MANAGER        = "O_Manager";
//	public static final String ATT_O_SUPERIOR	    = "O_Superior";
	

	//-------every entity class must initialize its DAO from here -----------------------------
	private 	static 	final boolean[] 			RIGHTS		= {true, true, true, true, false}; //canRead, canAdd, canUpd, canDel, del physique or flag only 
	private 	static	final boolean				API_CACHE 	= true;
	private 	static 	final boolean[]				HISTORY		= {false, false, false}; //add, mod, del

	public		static 	final EntityDAO<TaAutUser> 	DAO;
	static{
		DAO = new EntityDAO<TaAutUser>(Hnv_CfgHibernate.reqFactoryEMSession(Hnv_CfgHibernate.ID_FACT_MAIN), TaAutUser.class,RIGHTS,  HISTORY, DefDBExt.TA_AUT_USER, DefDBExt.ID_TA_AUT_USER);
	}

	//-----------------------Class Attributes-------------------------
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name=COL_I_ID, nullable = false)
	private	Integer         I_ID;

	@Column(name=COL_T_LOGIN_01, nullable = false)
	private	String          T_Login_01;

	@Column(name=COL_T_PASS_01, nullable = false)
	private	String          T_Pass_01;
	
	@Column(name=COL_T_LOGIN_02, nullable = false)
	private	String          T_Login_02;

	@Column(name=COL_T_PASS_02, nullable = false)
	private	String          T_Pass_02;

	@Column(name=COL_I_STATUS, nullable = true)
	private	Integer         I_Status;

	@Column(name=COL_I_TYPE_01, nullable = true)
	private	Integer         I_Type_01;

	@Column(name=COL_I_TYPE_02, nullable = true)
	private	Integer         I_Type_02;
	
	@Column(name=COL_D_DATE_01, nullable = true)
	private	Date            D_Date_01;

	@Column(name=COL_D_DATE_02, nullable = true)
	private	Date            D_Date_02;

	@Column(name=COL_D_DATE_03, nullable = true)
	private	Date            D_Date_03;

	@Column(name=COL_T_INFO_01, nullable = true)
	private	String            T_Info_01;

	@Column(name=COL_T_INFO_02, nullable = true)
	private	String            T_Info_02;

	@Column(name=COL_T_INFO_03, nullable = true)
	private	String            T_Info_03;

	@Column(name=COL_T_INFO_04, nullable = true)
	private	String            T_Info_04;

	@Column(name=COL_T_INFO_05, nullable = true)
	private	String            T_Info_05;

	@Column(name=COL_I_AUT_USER_01, nullable = true)
	private	Integer            I_Aut_User_01;

	@Column(name=COL_I_AUT_USER_02, nullable = true)
	private	Integer            I_Aut_User_02;

	@Column(name=COL_I_AUT_USER_03, nullable = true)
	private	Integer            I_Aut_User_03;

	@Column(name=COL_I_PER_MANAGER, nullable = true)
	private	Integer            I_Per_Manager;

	@Column(name=COL_I_PER_PERSON, nullable = true)
	private	Integer            I_Per_Person;
	//-----------------------Transient Variables-------------------------
	@Transient
	private	HashSet<Integer>  			O_Roles;

	@Transient
	private	HashSet<Integer> 			O_Rights;

	@Transient
	private TaTpyDocument 	            O_Avatar;
	
	@Transient
	private	List<TaTpyDocument> 		O_Documents;

	@Transient
	private	TaPerPerson					O_Per_Manager;
	
	@Transient
	private	TaPerPerson					O_Per_Person;

	@Transient
	private TaAutUser 	                O_Aut_Superior;
	
	//-----------------------------------------------------------------------
	private TaAutUser(){}

	public TaAutUser(Map<String, Object> attrs) throws Exception {
		this.reqSetAttrFromMap(attrs);
		//doInitDBFlag();
	}

	@Override
	public void doMergeWith(TaAutUser arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public Serializable reqRef() {
		// TODO Auto-generated method stub
		return null;
	}


	public Integer reqPerManagerId() {
		// TODO Auto-generated method stub
		return this.I_Per_Manager;
	}

	public Integer reqId() {
		// TODO Auto-generated method stub
		return this.I_ID;
	}

	public static TaAutUser reqUser(String login){
		if (login==null || login.length()==0) return null;		
		try {
			login 		= login.toLowerCase();
			List<TaAutUser> lU = TaAutUser.DAO.reqList(Restrictions.or(Restrictions.ilike(TaAutUser.ATT_T_LOGIN_01, login), Restrictions.ilike(TaAutUser.ATT_T_LOGIN_02, login)));
			if (lU!=null && lU.size()>0){						
				return lU.get(0);									
			}	
		} catch (Exception e) {			
			e.printStackTrace();
		}

		return null;
	}
	
	
	public static TaAutUser reqUserInfo(Integer id) throws Exception{
		if (id==null || id<=0) return null;
		
		TaAutUser user = TaAutUser.DAO.reqEntityByRef(id);
		
		if (user!=null) {
			user.reqSet(TaAutUser.ATT_T_PASS_01, null);
			user.reqSet(TaAutUser.ATT_T_PASS_02, null);
			user.reqSet(TaAutUser.ATT_D_DATE_01, null);
			user.reqSet(TaAutUser.ATT_D_DATE_02, null);
			user.reqSet(TaAutUser.ATT_D_DATE_03, null);
			user.reqSet(TaAutUser.ATT_I_STATUS, null);
		}
	

		return user;
	}
	

	public void doBuilRoleRighs(boolean forced) throws Exception {       
		if (this.O_Roles!=null && !forced) return;
		
		Criterion cri 		= Restrictions.eq(TaAutAuthorization.ATT_I_AUT_USER, this.I_ID);
		List<TaAutAuthorization> auths = TaAutAuthorization.DAO.reqList(cri);
		
		
		this.O_Roles 	= new HashSet<Integer>();
		this.O_Rights 	= new HashSet<Integer>();
		for	(TaAutAuthorization a : auths) {
			String 			r = a.reqStr(TaAutAuthorization.ATT_T_AUT_RIGHT);
			Set<Integer> 	s = ToolSet.reqSetInt(r);
			this.O_Rights.addAll(s);
			this.O_Roles .add(a.reqInt(TaAutAuthorization.ATT_I_AUT_ROLE));
		}
	}

	public boolean canHaveRight (Integer... rights) {
		if (rights!=null) {
			for (Integer r : rights)
				if (!O_Rights.contains(r)) return false;
		}
		return true;
	}
	
	public boolean canHaveRight (Set<Integer> rights) {
		if (rights!=null) {
			for (Integer r : rights)
				if (!O_Rights.contains(r)) return false;
		}
		return true;
	}
	
	public boolean canHaveOneRight (Integer... rights) {
		if (rights!=null) {
			for (Integer r : rights)
				if (O_Rights.contains(r)) return true;
		}
		return false;
	}
	
	public boolean canHaveRole (Integer... roles) {
		if (roles!=null) {
			for (Integer r : roles)
				if (!O_Roles.contains(r)) return false;
		}
		return true;
	}
	
	public boolean canHaveRole (Set<Integer> roles) {
		if (roles!=null) {
			for (Integer r : roles)
				if (!O_Rights.contains(r)) return false;
		}
		return true;
	}
	
	public boolean canHaveOneRole (Integer... roles) {
		if (roles!=null) {
			for (Integer r : roles)
				if (O_Rights.contains(r)) return true;
		}
		return false;
	}
	
	public boolean canHaveLogin(String login) {
		return this.T_Login_01.equals(login) || this.T_Login_02.equals(login);
	}
	
	private static List<Order> ordDocTyp = new ArrayList<Order>(Arrays.asList(Order.asc(TaTpyDocument.ATT_I_TYPE_01), Order.asc(TaTpyDocument.ATT_I_TYPE_02)));
	public void doBuildDocuments(boolean forced) {
		if (this.O_Documents != null && !forced) return;

		try {
			this.O_Documents = TaTpyDocument.DAO.reqList(
					ordDocTyp,
					Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_TYPE	, DefDBExt.ID_TA_PER_PERSON),
					Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_ID	, this.I_Per_Person)
					);
			if (this.O_Documents!=null && this.O_Documents.size()>0) {
				TaTpyDocument d = this.O_Documents.get(0);
				Integer typ01 = d.reqInt(TaTpyDocument.ATT_I_TYPE_01);
				Integer typ02 = d.reqInt(TaTpyDocument.ATT_I_TYPE_02);
				if (typ01!=null && typ02!=null && 
					typ01.equals(TaTpyDocument.TYPE_01_FILE_MEDIA) && 
					typ02.equals(TaTpyDocument.TYPE_02_FILE_IMG_AVATAR))
					this.O_Avatar = d;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doBuildAvatar(boolean forced) {
		if (this.O_Avatar != null && !forced) return;

		try {
			List<TaTpyDocument> lst = TaTpyDocument.DAO.reqList(
					Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_TYPE	, DefDBExt.ID_TA_PER_PERSON),
					Restrictions.eq(TaTpyDocument.ATT_I_ENTITY_ID	, this.I_Per_Person),
					Restrictions.eq(TaTpyDocument.ATT_I_TYPE_01		, TaTpyDocument.TYPE_01_FILE_MEDIA),
					Restrictions.eq(TaTpyDocument.ATT_I_TYPE_02		, TaTpyDocument.TYPE_02_FILE_IMG_AVATAR)
					);
			if (lst!=null && lst.size()>0) {
				this.O_Avatar = lst.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doBuildManager() {
		if (this.O_Per_Manager != null) return;

		try {
			this.O_Per_Manager = TaPerPerson.DAO.reqEntityByValue(TaPerPerson.ATT_I_ID, this.I_Per_Manager);
//			if (this.O_Per_Manager!=null)
//				this.O_Per_Manager.doBuildFiles(false, TaTpyDocument.TYPE_01_FILE_MEDIA, TaTpyDocument.TYPE_02_FILE_IMG_AVATAR );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doBuildPerson(boolean forced) {
		if (this.O_Per_Person != null && !forced) return;
		try {
			this.O_Per_Person = TaPerPerson.DAO.reqEntityByValue(TaPerPerson.ATT_I_ID, this.I_Per_Person);
			this.O_Per_Person.doBuildDocuments(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doBuildSuperior(boolean forced) {
		if (this.O_Aut_Superior != null && !forced) return;

		try {
			this.O_Aut_Superior = TaAutUser.DAO.reqEntityByValue(TaAutUser.ATT_I_ID, this.I_Aut_User_03);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_PASS_01, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_PASS_02, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_INFO_01, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_INFO_02, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_INFO_03, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_INFO_04, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_T_INFO_05, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_D_DATE_01, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_D_DATE_02, null);
			this.O_Aut_Superior.reqSet(TaAutUser.ATT_D_DATE_03, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean canBeSuperAdmin() {
		return this.I_Type_01.equals(TYPE_01_ADM_ALL);
	}
	
	public boolean canBeAdmin() {
		return this.I_Type_01.equals(TYPE_01_ADM);
	}
}
