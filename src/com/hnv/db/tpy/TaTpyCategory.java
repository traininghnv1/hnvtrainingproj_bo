package com.hnv.db.tpy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hnv.api.def.DefDBExt;
import com.hnv.api.main.Hnv_CfgHibernate;
import com.hnv.common.tool.ToolDBEntity;
import com.hnv.db.EntityAbstract;
import com.hnv.db.EntityDAO;

/**
* TaTpyCategory by H&V SAS
*/
@Entity
@Table(name = DefDBExt.TA_TPY_CATEGORY )
public class TaTpyCategory extends EntityAbstract<TaTpyCategory> {

	private static final long serialVersionUID = 1L;
	
	//---------------------------------------------------------------------------------
	public static final	int  TYPE_00_MATERIAL 	= 100;
	public static final	int  TYPE_00_POST 		= 200;
	
	//---------------------------List of Column from DB-----------------------------
	public static final String	COL_I_ID                              =	"I_ID";
	public static final String	COL_T_NAME                            =	"T_Name";
	public static final String	COL_T_CODE                            =	"T_Code";
	public static final String	COL_T_DESCRIPTION                     =	"T_Description";
	public static final String	COL_I_PER_MANAGER                     =	"I_Per_Manager";
	
	public static final String	COL_I_TYPE_00                     	  =	"I_Type_00"; // ID_Table using this cat
	public static final String	COL_I_TYPE_01                     	  =	"I_Type_01";
	public static final String	COL_I_TYPE_02                     	  =	"I_Type_02";

	public static final String	COL_I_PARENT                 		  =	"I_Parent"; //cat parent

	//---------------------------List of ATTR of class-----------------------------
	public static final String	ATT_I_ID                              =	"I_ID";
	public static final String	ATT_T_NAME                            =	"T_Name";
	public static final String	ATT_T_CODE                            =	"T_Code";
	public static final String	ATT_T_DESCRIPTION                     =	"T_Description";
	public static final String	ATT_I_PER_MANAGER                     =	"I_Per_Manager";
	

	public static final String	ATT_I_PARENT                          =	"I_Parent";
	
	public static final String	ATT_I_TYPE_00                     	  =	"I_Type_00";
	public static final String	ATT_I_TYPE_01                     	  =	"I_Type_01";
	public static final String	ATT_I_TYPE_02                     	  =	"I_Type_02";
	
	public static final String  ATT_O_CHILDREN                     	  =	"O_Children";
	public static final String  ATT_O_PARENT_NAME                  	  =	"O_Parent_Name";
	public static final String  ATT_O_DOCUMENTS                       =	"O_Documents";
	//-------every entity class must initialize its DAO from here -----------------------------
	private 	static 	final boolean[] 			RIGHTS		= {true, true, true, true, false}; //canRead, canAdd, canUpd, canDel, del physique or flag only 
	private 	static 	final boolean[]				HISTORY		= {false, false, false}; //add, mod, del

	public		static 	final EntityDAO<TaTpyCategory> 	DAO;
	static{
		DAO = new EntityDAO<TaTpyCategory>(Hnv_CfgHibernate.reqFactoryEMSession(Hnv_CfgHibernate.ID_FACT_MAIN) , TaTpyCategory.class, RIGHTS, HISTORY, DefDBExt.TA_TPY_CATEGORY, DefDBExt.ID_TA_TPY_CATEGORY);
	}

	//-----------------------Class Attributs-------------------------
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name=COL_I_ID, nullable = false)
	private	Integer         I_ID;
         
	@Column(name=COL_T_NAME, nullable = false)
	private	String          T_Name;
       
	@Column(name=COL_T_CODE, nullable = true)
	private	String          T_Code;
       
	@Column(name=COL_T_DESCRIPTION, nullable = true)
	private	String          T_Description;

	@Column(name=COL_I_PER_MANAGER, nullable = false)
	private	Integer         I_Per_Manager;

	@Column(name=COL_I_TYPE_00, nullable = false)
	private	Integer         I_Type_00;
	
	@Column(name=COL_I_PARENT, nullable = true)
	private	Integer         I_Parent;	
	
	@Column(name=COL_I_TYPE_01, nullable = true)
	private	Integer         I_Type_01;
	
	@Column(name=COL_I_TYPE_02, nullable = true)
	private	Integer         I_Type_02;
    
	@Transient
	private	List O_Children;
	
	@Transient
	private	String O_Parent_Name;
	
	@Transient
	private List<TaTpyDocument>			O_Documents;
	//-----------------------Transient Variables-------------------------


	//---------------------Constructeurs-----------------------
	private TaTpyCategory(){}

	public TaTpyCategory(Map<String, Object> attrs) throws Exception {
		this.reqSetAttrFromMap(attrs);
		doInitDBFlag();
	}
	
	public TaTpyCategory(String T_Name, Integer I_Per_Manager, Integer I_Type_00) throws Exception {
		this.reqSetAttr(
			ATT_T_NAME       , T_Name,
			ATT_I_PER_MANAGER, I_Per_Manager,
			ATT_I_TYPE_00	 , I_Type_00
		);
		doInitDBFlag();
	}
	public TaTpyCategory(String T_Name, String T_Code, String T_Description, Integer I_Per_Manager, Integer I_Type_00) throws Exception {
		this.reqSetAttr(
			ATT_T_NAME                 , T_Name,
			ATT_T_CODE                 , T_Code,
			ATT_T_DESCRIPTION          , T_Description,
			ATT_I_PER_MANAGER          , I_Per_Manager,
			ATT_I_TYPE_00          		, I_Type_00
		);
		doInitDBFlag();
	}
	
	
	//---------------------EntityInterface-----------------------
	@Override
	public Serializable reqRef() {
		return this.I_ID;

	}

	@Override
	public void doMergeWith(TaTpyCategory ent) {
		if (ent == this) return;
		this.T_Name                 = ent.T_Name;
		this.T_Code                 = ent.T_Code;
		this.T_Description          = ent.T_Description;
		this.I_Per_Manager          = ent.I_Per_Manager;
		this.I_Type_00          = ent.I_Type_00;

		this.I_Type_01         	 	= ent.I_Type_01;
		this.I_Type_02          	= ent.I_Type_02;

		//---------------------Merge Transient Variables if exist-----------------------
	}

	@Override
	public boolean equals(Object o)  {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		boolean ok = false;
		
		ok = (I_ID == ((TaTpyCategory)o).I_ID);
		if (!ok) return ok;

				
		if (!ok) return ok;
		return ok;
	}

	@Override
	public int hashCode() {
		return this.I_ID;
	}

	
	public void doBuildDocuments(boolean forced) throws Exception{
		if (!forced){
			return;
		}
		
		O_Documents = TaTpyDocument.reqTpyDocuments(DefDBExt.ID_TA_TPY_CATEGORY, I_ID, null, null);
		
		if (O_Documents!=null) {
			for (TaTpyDocument doc : O_Documents) {
				doc.reqSet(TaTpyDocument.ATT_T_INFO_02 , null); //hide to client the path real in server
			}
		}
	}
	
	public void doAddChild(TaTpyCategory child) {
		if (O_Children==null)
			O_Children = new ArrayList<TaTpyCategory>();
		
		O_Children.add(child);
		
		child.O_Parent_Name = this.T_Name;
	}
	
	//-------------------------------------------------------------------------------------------------------
	public static List<TaTpyCategory> reqListByType (Integer type, Integer manId) throws Exception{		
		Criterion cri = Restrictions.gt("I_ID", 0);	

		if (type!=null)
			cri = Restrictions.and(cri, Restrictions.eqOrIsNull(TaTpyCategory.ATT_I_TYPE_00, type));

		if (manId!=null)
			cri = Restrictions.and(cri, Restrictions.eqOrIsNull(TaTpyCategory.ATT_I_PER_MANAGER, manId));
		
		List lst = TaTpyCategory.DAO.reqList(Order.asc(TaTpyCategory.ATT_T_CODE), cri);
				
		return lst;
	}

	public static Map<Integer, TaTpyCategory> reqMapByType (Integer type, Integer manId) throws Exception{		
		Criterion cri = Restrictions.gt("I_ID", 0);	

		if (type!=null)
			cri = Restrictions.and(cri, Restrictions.eqOrIsNull(TaTpyCategory.ATT_I_TYPE_00, type));

		if (manId!=null)
			cri = Restrictions.and(cri, Restrictions.eqOrIsNull(TaTpyCategory.ATT_I_PER_MANAGER, manId));
		
		return TaTpyCategory.DAO.reqMap(cri);
	}
	
	public static List<TaTpyCategory> reqListMultiLevel (Integer type, Integer manId) throws Exception{		
		Criterion cri = Restrictions.gt("I_ID", 0);	

		if (type!=null)
			cri = Restrictions.and(cri, Restrictions.eqOrIsNull(TaTpyCategory.ATT_I_TYPE_00, type));

		if (manId!=null)
			cri = Restrictions.and(cri, Restrictions.eqOrIsNull(TaTpyCategory.ATT_I_PER_MANAGER, manId));
		
		List<TaTpyCategory> lst = TaTpyCategory.DAO.reqList(Order.asc(TaTpyCategory.ATT_T_CODE), cri);
		
		ToolDBEntity.reqTabKeyInt(lst, TaTpyCategory.ATT_I_ID);
		
		List lstRS = new ArrayList<TaTpyCategory>();
		
		for (TaTpyCategory cat : lst) {
			Integer pId = (Integer) cat.req(TaTpyCategory.ATT_I_PARENT);
			if (pId == null) 
				lstRS.add(cat);
			else {
				TaTpyCategory par = (TaTpyCategory)lstRS.get(pId);
				par.doAddChild(cat);
			}
		}
		
		return lstRS;
	}
	
}
